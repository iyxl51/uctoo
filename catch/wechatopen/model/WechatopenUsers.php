<?php

namespace catchAdmin\wechatopen\model;

use catcher\base\CatchModel as Model;
/**
 *
 * @property int $id
 * @property string $user_ids
 * @property string $appid
 * @property string $openid
 * @property string $unionid
 * @property string $nickname
 * @property string $mobile
 * @property string $password
 * @property string $headimgurl
 * @property int $sex
 * @property string $country
 * @property string $province
 * @property string $city
 * @property string $language
 * @property int $subscribe
 * @property int $subscribe_time
 * @property string $remark
 * @property int $groupid
 * @property string $tagid_list
 * @property string $subscribe_scene
 * @property string $qr_scene
 * @property string $qr_scene_str
 * @property string $privilege
 * @property string $loginip
 * @property string $token
 * @property string $status
 * @property string $access_token
 * @property int $access_token_overtime
 * @property string $refresh_token
 * @property int $refresh_token_overtime
 * @property string $verification
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 * @property int $creator_id
 */
class WechatopenUsers extends Model
{
    // 表名
    public $name = 'wechatopen_users';
    // 数据库字段映射
    public $field = array(
        'id',
        // 关联users表ID
        'user_ids',
        // 关联applet表appid
        'appid',
        // 普通用户的标识
        'openid',
        // 用户统一标识
        'unionid',
        // 普通用户昵称
        'nickname',
        // 手机号码
        'mobile',
        // 登录密码
        'password',
        // 用户头像
        'headimgurl',
        // 普通用户性别，1=男性，2=女性
        'sex',
        // 国家
        'country',
        // 省份
        'province',
        // 城市
        'city',
        // 用户的语言
        'language',
        // 是否订阅该公众号标识，0=未关注，1=关注
        'subscribe',
        // 用户关注时间，为时间戳
        'subscribe_time',
        // 公众号运营者对粉丝的备注
        'remark',
        // 用户所在的分组ID（兼容旧的用户分组接口）
        'groupid',
        // 用户被打上的标签ID列表
        'tagid_list',
        // 返回用户关注的渠道来源
        'subscribe_scene',
        // 二维码扫码场景（开发者自定义）
        'qr_scene',
        // 二维码扫码场景描述（开发者自定义）
        'qr_scene_str',
        // 用户特权信息，json数组
        'privilege',
        // IP地址
        'loginip',
        // token
        'token',
        // 状态
        'status',
        // access_token
        'access_token',
        // access_token_overtime
        'access_token_overtime',
        // refresh_token
        'refresh_token',
        // refresh_token_overtime
        'refresh_token_overtime',
        // 验证
        'verification',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除字段
        'deleted_at',
        // 创建人ID
        'creator_id',
    );
}