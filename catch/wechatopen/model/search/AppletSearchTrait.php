<?php
/**
 * @filename  UserSearchTrait.php
 * @createdAt 2020/6/21
 * @project  https://github.com/yanwenwu/catch-admin
 * @document http://doc.catchadmin.com
 * @author   JaguarJack <njphper@gmail.com>
 * @copyright By CatchAdmin
 * @license  https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt
 */
namespace catchAdmin\wechatopen\model\search;

use catchAdmin\permissions\model\Users;

trait AppletSearchTrait
{
    /**
     * 名称搜索
     * @time 2021年05月22日
     * @param $query
     * @param $value
     * @param $data
     * @return mixed
     */
    public function searchNameAttr($query, $value, $data)
    {
        return $query->whereLike('name', $value);
    }

    /**
     * appid搜索
     *
     * @time 2021年05月22日
     * @param $query
     * @param $value
     * @param $data
     * @return mixed
     */
    public function searchAppidAttr($query, $value, $data)
    {
        return $query->where('appid', $value);
    }

    /**
     * 创建人搜索
     *
     * @time 2021年05月22日
     * @param $query
     * @param $value
     * @param $data
     * @return mixed
     */
    public function searchCreatorAttr($query, $value, $data)
    {
        return $query->where(app(Users::class)->getTable() . '.username', $value);
    }

}