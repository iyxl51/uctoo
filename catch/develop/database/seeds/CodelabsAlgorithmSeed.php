<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~{$year} http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: UCToo [ contact@uctoo.com ]
// +----------------------------------------------------------------------

use think\migration\Seeder;

class CodelabsAlgorithmSeed extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
       $data = array (
  0 => 
  array (
    'id' => 1,
    'command_name' => 'create:elmform',
    'algorithm_code' => NULL,
    'filename' => 'CreateElmformCommand.php',
    'filepath' => '/extend/catcher/command/Tools/',
    'template_url' => '',
    'opensource' => 1,
    'author' => 'UCToo',
    'author_url' => '',
    'user_id' => 1,
    'tags' => 'lowcode',
    'docs' => '无',
    'images' => '',
    'description' => '介绍',
    'price' => 0,
    'status' => 1,
    'arguments' => '',
    'created_at' => 1635997529,
    'updated_at' => 1635997529,
    'deleted_at' => 0,
    'creator_id' => 1,
  ),
);

        foreach ($data as $item) {
            \catchAdmin\develop\model\CodelabsAlgorithm::create($item);
        }
    }
}