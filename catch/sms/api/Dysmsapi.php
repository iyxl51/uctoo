<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Patrick <contact@uctoo.com>
// +----------------------------------------------------------------------

namespace catchAdmin\sms\api;

use AlibabaCloud\Client\AlibabaCloud;
use app\BaseController;
use catchAdmin\wechatopen\model\Applet;
use catchAdmin\sms\Sms as Smsmodule;
use catcher\CatchResponse;
use catcher\Code;
use think\facade\Log;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;
use catchAdmin\sms\model\SmsLog;

class Dysmsapi extends BaseController
{
    /**
     * applet 实例
     * @var \catchAdmin\wechatopen\model\Applet
     */
    protected $applet;

    // 初始化
    protected function initialize()
    {
        $appid = input('appid');
        $this->applet = Applet::where('appid',$appid)->find();
    }

    /**
     * 发送短信
     * @param string $phone
     * @param array $data
     * @return mixed
     * @param Smsmodule $Smsapp
     * @return mixed
     * @throws \Overtrue\EasySms\Exceptions\NoGatewayAvailableException
     * @throws \Overtrue\EasySms\Exceptions\InvalidArgumentException
     */
    public function SendSms(){
        $apiconfig = Smsmodule::__callStatic('aliyun',[]);
        AlibabaCloud::accessKeyClient($apiconfig['gateways']['aliyun']['access_key_id'], $apiconfig['gateways']['aliyun']['access_key_secret'])->asDefaultClient();
        $TemplateCode = input('TemplateCode');
        $PhoneNumbers = input('PhoneNumbers');
        $SignName = input('SignName');
        $TemplateParam = input('TemplateParam');

        $result = null;
        try {
            $result = AlibabaCloud::rpc()
                ->regionId('cn-hangzhou')
                ->product('Dysmsapi')
                ->version('2017-05-25')
                ->action('SendSms')
                ->method('POST')
                ->withTemplateCode($TemplateCode)
                ->withPhoneNumbers($PhoneNumbers)
                ->withSignName($SignName)
                ->withTemplateParam($TemplateParam)
                ->request();

            Log::write($result->toArray(),'debug');
            //添加短信记录
            SmsLog::create(['event'=>input('event'),
                            'mobile'=>$PhoneNumbers,
                            'code'=>json_decode($TemplateParam,true)['code'],
                            'times'=>0,
                            'ip'=>request()->ip()
                ]);

            $this->success('ok',$result,200);

        } catch (ClientException $exception) {
            Log::write($exception->getErrorMessage(),'debug');
        } catch (ServerException $exception) {
            Log::write($exception->getErrorMessage(),'debug');
        }
        Log::write($result,'debug');
    }

    /**
     * 检测验证码
     *
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function smscheck(){
        $captcha = input('captcha/s',false);
        $mobile = input('mobile/s',false);
        $event = input('event/s',false);
        if(!$captcha || !$mobile){
            return CatchResponse::fail('参数错误',Code::PARAM_ERROR);
        }
        $ret = SmsLog::check($mobile, $captcha, $event);
        if (!$ret) {
            return CatchResponse::fail('验证码不正确',Code::VALIDATE_FAILED);
        }
        return CatchResponse::success(['ok']);
    }
}