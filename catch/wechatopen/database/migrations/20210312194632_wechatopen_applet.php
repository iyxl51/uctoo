<?php

use think\migration\Migrator;
use think\migration\db\Column;
use Phinx\Db\Adapter\MysqlAdapter;

class WechatopenApplet extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('wechatopen_applet', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '微信应用信息表' ,'id' => 'id','signed' => true ,'primary_key' => ['id']]);
        $table->addColumn('name', 'string', ['limit' => 100,'null' => false,'default' => '','signed' => true,'comment' => '应用名称',])
			->addColumn('typedata', 'string', ['limit' => 50,'null' => false,'default' => '','signed' => true,'comment' => '应用类型',])
			->addColumn('token', 'string', ['limit' => 100,'null' => true,'default' => '','signed' => true,'comment' => 'Token',])
			->addColumn('appid', 'string', ['limit' => 100,'null' => false,'default' => '','signed' => true,'comment' => 'appid',])
			->addColumn('appsecret', 'string', ['limit' => 255,'null' => true,'default' => '','signed' => true,'comment' => 'appsecret',])
			->addColumn('aeskey', 'string', ['limit' => 255,'null' => true,'default' => '','signed' => true,'comment' => 'EncodingAESKey',])
			->addColumn('mchid', 'string', ['limit' => 50,'null' => true,'default' => '','signed' => true,'comment' => '微信支付商户号',])
			->addColumn('mchkey', 'string', ['limit' => 50,'null' => true,'default' => '','signed' => true,'comment' => '商户支付密钥',])
			->addColumn('mch_api_cert', 'string', ['limit' => 255,'null' => true,'default' => '','signed' => true,'comment' => '商户API证书cert',])
			->addColumn('mch_api_key', 'string', ['limit' => 255,'null' => true,'default' => '','signed' => true,'comment' => '商户API证书key',])
			->addColumn('notify_url', 'string', ['limit' => 255,'null' => true,'default' => '','signed' => true,'comment' => '微信支付异步通知url',])
			->addColumn('principal', 'string', ['limit' => 100,'null' => true,'default' => '','signed' => true,'comment' => '主体名称',])
			->addColumn('original', 'string', ['limit' => 50,'null' => true,'default' => '','signed' => true,'comment' => '原始ID',])
			->addColumn('wechat', 'string', ['limit' => 50,'null' => true,'default' => '','signed' => true,'comment' => '微信号',])
			->addColumn('headface_image', 'string', ['limit' => 255,'null' => true,'default' => '','signed' => true,'comment' => '头像',])
			->addColumn('qrcode_image', 'string', ['limit' => 255,'null' => true,'default' => '','signed' => true,'comment' => '二维码图片',])
			->addColumn('signature', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR,'null' => true,'signed' => true,'comment' => '账号介绍',])
			->addColumn('city', 'string', ['limit' => 50,'null' => true,'default' => '','signed' => true,'comment' => '省市',])
			->addColumn('status', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 1,'signed' => true,'comment' => '状态',])
			->addColumn('weigh', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => true,'default' => 0,'signed' => true,'comment' => '权重',])
			->addColumn('service_type_info', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => true,'default' => 0,'signed' => true,'comment' => '授权方公众号类型',])
			->addColumn('verify_type_info', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => true,'default' => 0,'signed' => true,'comment' => '授权方认证类型',])
			->addColumn('business_info', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR,'null' => true,'signed' => true,'comment' => '用以了解公众号功能的开通状况',])
			->addColumn('authorizer_access_token', 'string', ['limit' => 255,'null' => true,'default' => '','signed' => true,'comment' => '第三方平台授权token',])
			->addColumn('access_token_overtime', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => true,'default' => 0,'signed' => true,'comment' => '授权token过期时间',])
			->addColumn('authorizer_refresh_token', 'string', ['limit' => 255,'null' => true,'default' => '','signed' => true,'comment' => '授权刷新token',])
			->addColumn('miniprograminfo', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR,'null' => true,'signed' => true,'comment' => '小程序信息',])
			->addColumn('func_info', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR,'null' => true,'signed' => true,'comment' => '公众号授权给开发者的权限集列表',])
			->addColumn('ticket', 'string', ['limit' => 100,'null' => true,'default' => '','signed' => true,'comment' => 'jsapi ticket',])
			->addColumn('ticket_overtime', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => true,'default' => 0,'signed' => true,'comment' => 'jsapi ticket 过期时间',])
			->addColumn('creator_id', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建人ID',])
			->addColumn('created_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建时间',])
			->addColumn('updated_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '更新时间',])
			->addColumn('deleted_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '软删除',])
			->addIndex(['appid'], ['unique' => true,'name' => 'unique_appid'])
            ->create();
    }
}
