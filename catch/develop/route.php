<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~{$year} http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: JaguarJack [ njphper@gmail.com ]
// +----------------------------------------------------------------------
// you should use `$router`
/* @var think\Route $router */
$router->group(function () use($router) {
    // codelabs 路由
    $router->resource('codelabs', catchAdmin\develop\controller\Codelabs::class);
    // 获取最新一条数据
    $router->get('codelabsLatest', '\catchAdmin\develop\controller\Codelabs@latest');
    // codelabsTemplates 路由
    $router->resource('codelabsTemplates', catchAdmin\develop\controller\CodelabsTemplates::class);
    // codelabsAlgorithm 路由
     $router->resource('codelabsAlgorithm', catchAdmin\develop\controller\CodelabsAlgorithm::class);
    // 代码生成
    $router->post('codelabsGenerate', '\catchAdmin\develop\controller\Codelabs@generate');
    // 代码保存
    $router->put('codelabsSavefile/<id>', '\catchAdmin\develop\controller\Codelabs@savefile');
    // 分享代码
    $router->post('codelabsShare', '\catchAdmin\develop\controller\Codelabs@share');
})->middleware('auth');