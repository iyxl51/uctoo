<?php

namespace catchAdmin\test\controller;

use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\test\model\Test as TestModel;
use think\Response;

class Test extends CatchController
{
    
    protected $testModel;
    
    /**
     *
     * @time 2021/10/18 16:51
     * @param TestModel $testModel
     * @return mixed
     */
    public function __construct(TestModel $testModel)
    {
        $this->testModel = $testModel;
    }
    
    /**
     *
     * @time 2021/10/18 16:51
     * @return Response
     */
    public function index() : Response
    {
        return CatchResponse::paginate($this->testModel->getList());
    }
    
    /**
     *
     * @time 2021/10/18 16:51
     * @param Request $request
     * @return Response
     */
    public function save(Request $request) : Response
    {
        return CatchResponse::success($this->testModel->storeBy($request->post()));
    }
    
    /**
     *
     * @time 2021/10/18 16:51
     * @param $id
     * @return Response
     */
    public function read($id) : Response
    {
        return CatchResponse::success($this->testModel->findBy($id));
    }
    
    /**
     *
     * @time 2021/10/18 16:51
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request) : Response
    {
        return CatchResponse::success($this->testModel->updateBy($id, $request->post()));
    }
    
    /**
     *
     * @time 2021/10/18 16:51
     * @param $id
     * @return Response
     */
    public function delete($id) : Response
    {
        return CatchResponse::success($this->testModel->deleteBy($id));
    }
}