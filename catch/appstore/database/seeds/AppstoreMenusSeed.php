<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: UCToo <contact@uctoo.com>
// +----------------------------------------------------------------------

use think\facade\Log;
use think\migration\Seeder;

class AppstoreMenusSeed extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        Log::write("AppstoreMenusSeed",'debug');
        \catcher\Utils::importTreeData($this->getPermissions(), 'permissions', 'parent_id');
    }

    protected function getPermissions()
    {
       return array (
  0 => 
  array (
    'id' => 117,
    'permission_name' => '应用市场',
    'parent_id' => 0,
    'level' => '',
    'route' => '/appstore',
    'icon' => 'el-icon-cloudy',
    'module' => 'appstore',
    'creator_id' => 1,
    'permission_mark' => 'appstore',
    'component' => 'layout',
    'redirect' => '',
    'keepalive' => 1,
    'type' => 1,
    'hidden' => 1,
    'sort' => 1,
    'created_at' => 1620712000,
    'updated_at' => 1620712000,
    'deleted_at' => 0,
    'children' => 
    array (
      0 => 
      array (
        'id' => 118,
        'permission_name' => '应用市场',
        'parent_id' => 117,
        'level' => '',
        'route' => 'https://appstore.uctoo.com',
        'icon' => 'el-icon-download',
        'module' => 'appstore',
        'creator_id' => 1,
        'permission_mark' => 'appstoreindex',
        'component' => 'layout',
        'redirect' => '',
        'keepalive' => 1,
        'type' => 1,
        'hidden' => 1,
        'sort' => 1,
        'created_at' => 1620712000,
        'updated_at' => 1620712000,
        'deleted_at' => 0,
      ),
      1 => 
      array (
        'id' => 119,
        'permission_name' => '应用市场管理',
        'parent_id' => 117,
        'level' => '',
        'route' => '/appstore/management',
        'icon' => 'el-icon-s-fold',
        'module' => 'appstore',
        'creator_id' => 1,
        'permission_mark' => 'management',
        'component' => 'appstoreManagement',
        'redirect' => '',
        'keepalive' => 1,
        'type' => 1,
        'hidden' => 1,
        'sort' => 1,
        'created_at' => 1620712000,
        'updated_at' => 1620712000,
        'deleted_at' => 0,
      ),
    ),
  ),
);
    }
}
