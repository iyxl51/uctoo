<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Patrick <contact@uctoo.com>
// +----------------------------------------------------------------------

namespace app\controller;


use app\BaseController;
use uctoo\event\InvokeCloudFunction;
use uctoo\event\InvokeCloudFunctionBefore;
use uctoo\middleware\Applet;
use uctoo\middleware\Auth;
use app\model\saas\MerchantAccount;
use uctoo\ThinkEasyWeChat\MiniProgram\CloudFile\Client;
use uctoo\ThinkEasyWeChat\MiniProgram\CloudFile\ServiceProvider as CloudFileServiceProvider;
use uctoo\ThinkEasyWeChat\MiniProgram\CloudFunction\ServiceProvider as CloudFunctionServiceProvider;
use uctoo\validate\File;
use EasyWeChat\OpenPlatform\Application;
use think\exception\ValidateException;
use think\facade\Config;
use think\facade\Db;
use think\facade\Event;
use think\facade\Validate;

class MiniProgram extends BaseController
{

    protected $middleware = [
        Auth::class,
        Applet::class
    ];

    public function invokeFunction(){
        $name = $this->request->post('func/s',false);
        $postBody = $this->request->post('body',false);
        if(!$name || !$postBody){
             $this->error('缺少参数');
        }
        \event(new InvokeCloudFunctionBefore($name,$postBody));
        try{
            /**
             * @var \EasyWeChat\MiniProgram\Application $mapp
             */
            $mapp = $this->request->mapp;
            $mapp->registerProviders([CloudFunctionServiceProvider::class]);
            $res = $mapp->cloud_function->invokeFunction($name,$postBody);
        }catch (\Exception $e){
            // TODO 记录日志
            $this->error('系统错误，请联系管理员');
        }
        $this->wxResult($res,'invoke_function',function (&$data)use($name,$postBody){
            $data = json_decode($data['resp_data'],true);
            \event(new InvokeCloudFunction($name,$postBody,$data));
        });
    }

    /**
     * 上传临时素材
     */
    public function uploadFile(){
        $path = $this->request->post('path/s',false);
        if(!$path){
            $this->error('缺少参数');
        }
        $file = $this->request->file('file');
        if (empty($file)) {
            $this->error('文件错误');
        }
        try{
            $this->validate(['file'=>$file],File::class);

            $this->request->mapp->registerProviders([CloudFileServiceProvider::class]);
            /**
             * @var Client $cloudFile
             */
            $cloudFile = $this->request->mapp->cloud_file;
            $path = trim($path,'/').'/'.md5((string) microtime(true)). '.' . $file->extension();
            $res = $cloudFile->uploadFile($path,$file->getRealPath());
        }catch (ValidateException $e){
            $this->error($e->getMessage());
        }catch (\Exception $e){
            $this->error('上传失败');
        }
        $this->success('success',$res);

    }

    public function batchDownloadFile(){
        $file_list = $this->request->post('file_list',[]);
        if(!$file_list){
              $this->error('缺少参数');
        }
        $this->request->mapp->registerProviders([CloudFileServiceProvider::class]);
        /**
         * @var Client $cloudFile
         */
        $cloudFile = $this->request->mapp->cloud_file;
        $res = $cloudFile->batchDownloadFile($file_list);
        $this->wxResult($res);
    }

    public function batchDeleteFile(){
        $fileid_list = $this->request->post('fileid_list',[]);
        if(!$fileid_list){
              $this->error('缺少参数');
        }
        $this->request->mapp->registerProviders([CloudFileServiceProvider::class]);
        /**
         * @var Client $cloudFile
         */
        $cloudFile = $this->request->mapp->cloud_file;
        $res = $cloudFile->batchDeleteFile($fileid_list);
        $this->wxResult($res);
    }


}