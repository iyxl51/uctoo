<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~{$year} http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: UCToo [ contact@uctoo.com ]
// +----------------------------------------------------------------------

use think\migration\Seeder;

class CodelabsTemplatesSeed extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
       $data = array (
  0 => 
  array (
    'id' => 1,
    'name' => 'UCToo表格CURD',
    'template_code' => NULL,
    'filename' => 'elmform.stub',
    'filepath' => '/extend/catcher/command/stubs/elm/',
    'template_url' => '',
    'opensource' => 1,
    'author' => 'UCToo',
    'author_url' => 'https://www.uctoo.com',
    'user_id' => 1,
    'tags' => 'table',
    'docs' => '',
    'images' => '',
    'description' => 'headless CMS 默认CURD页面',
    'price' => 0,
    'status' => 1,
    'created_at' => 1635928574,
    'updated_at' => 1635928986,
    'deleted_at' => 0,
    'creator_id' => 1,
  ),
);

        foreach ($data as $item) {
            \catchAdmin\develop\model\CodelabsTemplates::create($item);
        }
    }
}