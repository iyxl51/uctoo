<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Patrick <contact@uctoo.com>
// +----------------------------------------------------------------------

namespace uctoo\util\library;


use EasyWeChat\Factory;
use EasyWeChat\OpenPlatform\Application;

/**
 * Class Mpopen
 * @mixin Application
 * @package app\util
 */
class Mpopen
{
    public static function __make(){
        $app =  Factory::openPlatform(config('wechat.open_platform.default'));//TODO:从管理后台配置获取
        $app->rebind('cache',app('cache'));
        return $app;
    }
}