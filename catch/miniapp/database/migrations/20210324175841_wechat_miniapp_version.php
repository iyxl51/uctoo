<?php

use think\migration\Migrator;
use think\migration\db\Column;
use Phinx\Db\Adapter\MysqlAdapter;

class WechatMiniappVersion extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('wechat_miniapp_version', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '小程序版本管理' ,'id' => 'id','signed' => true ,'primary_key' => ['id']]);
        $table->addColumn('product_id', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => true,'signed' => true,'comment' => 'SaaS产品ID',])
			->addColumn('template_id', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => true,'comment' => '第三方平台代码库中的代码模版ID',])
			->addColumn('ext_json', 'json', ['null' => true,'signed' => true,'comment' => '第三方模板自定义的配置',])
			->addColumn('user_version', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '代码版本号，开发者可自定义',])
			->addColumn('user_desc', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '代码描述，开发者可自定义',])
			->addColumn('status', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => true,'comment' => '代码状态:-1=已下线,0=未上传,1=已上传,2=已发布',])
			->addColumn('audit_status', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => true,'comment' => '审核状态:-1=未提交审核,0=审核成功,1=审核失败,2=审核中',])
			->addColumn('category_list', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR,'null' => true,'signed' => true,'comment' => '可填选的类目列表',])
			->addColumn('page_list', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR,'null' => true,'signed' => true,'comment' => '页面配置列表',])
			->addColumn('item_list', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR,'null' => true,'signed' => true,'comment' => '提交审核项的一个列表（至少填写1项，至多填写5项）',])
			->addColumn('auditid', 'string', ['limit' => 16,'null' => true,'signed' => true,'comment' => '提交审核时获得的审核id',])
			->addColumn('reason', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR,'null' => true,'signed' => true,'comment' => '审核不通过原因',])
			->addColumn('succ_time', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => true,'signed' => true,'comment' => '审核成功时间',])
			->addColumn('fail_time', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => true,'signed' => true,'comment' => '审核失败时间',])
			->addColumn('appid', 'string', ['limit' => 255,'null' => false,'default' => '','signed' => true,'comment' => 'appid',])
			->addColumn('creator_id', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建人ID',])
			->addColumn('created_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建时间',])
			->addColumn('updated_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '更新时间',])
			->addColumn('deleted_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '软删除',])
            ->create();
    }
}
