<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~{$year} http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: UCToo [ contact@uctoo.com ]
// +----------------------------------------------------------------------

use think\migration\Seeder;

class ConfigSeed extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
       $data = array (
  0 => 
  array (
    'id' => 1,
    'name' => '基础配置',
    'pid' => 0,
    'component' => 'basic',
    'key' => 'basic',
    'value' => '',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1612094907,
    'updated_at' => 1612094907,
    'deleted_at' => 0,
  ),
  1 => 
  array (
    'id' => 2,
    'name' => '上传配置',
    'pid' => 0,
    'component' => 'upload',
    'key' => 'upload',
    'value' => '',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1612094907,
    'updated_at' => 1612094907,
    'deleted_at' => 0,
  ),
  2 => 
  array (
    'id' => 3,
    'name' => '',
    'pid' => 1,
    'component' => '',
    'key' => 'site.name',
    'value' => 'UCToo',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1612095429,
    'updated_at' => 1637040687,
    'deleted_at' => 0,
  ),
  3 => 
  array (
    'id' => 4,
    'name' => '',
    'pid' => 1,
    'component' => '',
    'key' => 'site.url',
    'value' => '',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1612095429,
    'updated_at' => 1612095429,
    'deleted_at' => 0,
  ),
  4 => 
  array (
    'id' => 5,
    'name' => '',
    'pid' => 1,
    'component' => '',
    'key' => 'site.start_at',
    'value' => '',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1612095429,
    'updated_at' => 1612095429,
    'deleted_at' => 0,
  ),
  5 => 
  array (
    'id' => 6,
    'name' => '',
    'pid' => 1,
    'component' => '',
    'key' => 'site.keywords',
    'value' => 'UCToo',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1612095429,
    'updated_at' => 1637041172,
    'deleted_at' => 0,
  ),
  6 => 
  array (
    'id' => 7,
    'name' => '',
    'pid' => 1,
    'component' => '',
    'key' => 'site.description',
    'value' => '',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1612095429,
    'updated_at' => 1612095429,
    'deleted_at' => 0,
  ),
  7 => 
  array (
    'id' => 8,
    'name' => '',
    'pid' => 1,
    'component' => '',
    'key' => 'site.upload',
    'value' => 'local',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1612095429,
    'updated_at' => 1612095429,
    'deleted_at' => 0,
  ),
  8 => 
  array (
    'id' => 9,
    'name' => '',
    'pid' => 1,
    'component' => '',
    'key' => 'footer.record_number',
    'value' => 'xxx号',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1612095429,
    'updated_at' => 1612687562,
    'deleted_at' => 0,
  ),
  9 => 
  array (
    'id' => 10,
    'name' => '',
    'pid' => 1,
    'component' => '',
    'key' => 'footer.copyright',
    'value' => 'UCToo All rights reserved.',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1612095429,
    'updated_at' => 1612687562,
    'deleted_at' => 0,
  ),
  10 => 
  array (
    'id' => 11,
    'name' => '',
    'pid' => 1,
    'component' => '',
    'key' => 'footer.contact',
    'value' => 'QQ 844801465',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1612095429,
    'updated_at' => 1612687562,
    'deleted_at' => 0,
  ),
  11 => 
  array (
    'id' => 12,
    'name' => '',
    'pid' => 1,
    'component' => '',
    'key' => 'footer.record_url',
    'value' => 'https://beian.miit.gov.cn/',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1612095429,
    'updated_at' => 1612687562,
    'deleted_at' => 0,
  ),
  12 => 
  array (
    'id' => 13,
    'name' => '',
    'pid' => 1,
    'component' => '',
    'key' => 'other.blacklist',
    'value' => '',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1612095429,
    'updated_at' => 1612095429,
    'deleted_at' => 0,
  ),
  13 => 
  array (
    'id' => 14,
    'name' => '',
    'pid' => 1,
    'component' => '',
    'key' => 'site.cleardashboard',
    'value' => '1',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1612095429,
    'updated_at' => 1637043797,
    'deleted_at' => 0,
  ),
  14 => 
  array (
    'id' => 15,
    'name' => '微信配置',
    'pid' => 0,
    'component' => 'wechat',
    'key' => 'wechat',
    'value' => '',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1612094907,
    'updated_at' => 1612094907,
    'deleted_at' => 0,
  ),
  15 => 
  array (
    'id' => 16,
    'name' => '',
    'pid' => 15,
    'component' => '',
    'key' => 'mpopen.appid',
    'value' => '',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1622202840,
    'updated_at' => 1622202840,
    'deleted_at' => 0,
  ),
  16 => 
  array (
    'id' => 17,
    'name' => '',
    'pid' => 15,
    'component' => '',
    'key' => 'website.appid',
    'value' => '',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1622202840,
    'updated_at' => 1622202840,
    'deleted_at' => 0,
  ),
  17 => 
  array (
    'id' => 18,
    'name' => '',
    'pid' => 15,
    'component' => '',
    'key' => 'website.appsecret',
    'value' => '',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1622202840,
    'updated_at' => 1622202840,
    'deleted_at' => 0,
  ),
  18 => 
  array (
    'id' => 19,
    'name' => '',
    'pid' => 15,
    'component' => '',
    'key' => 'wechatopen.host',
    'value' => 'https://serv.uctoo.com',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1622202840,
    'updated_at' => 1622202840,
    'deleted_at' => 0,
  ),
  19 => 
  array (
    'id' => 20,
    'name' => '',
    'pid' => 15,
    'component' => '',
    'key' => 'wechatopen.account',
    'value' => '',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1622202840,
    'updated_at' => 1622202840,
    'deleted_at' => 0,
  ),
  20 => 
  array (
    'id' => 21,
    'name' => '',
    'pid' => 15,
    'component' => '',
    'key' => 'wechatopen.password',
    'value' => '',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1622202840,
    'updated_at' => 1622202840,
    'deleted_at' => 0,
  ),
  21 => 
  array (
    'id' => 22,
    'name' => '',
    'pid' => 26,
    'component' => '',
    'key' => 'appstoreserver.host',
    'value' => 'https://serv.uctoo.com',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1622202840,
    'updated_at' => 1622202840,
    'deleted_at' => 0,
  ),
  22 => 
  array (
    'id' => 23,
    'name' => '腾讯云配置',
    'pid' => 0,
    'component' => 'tencentcloud',
    'key' => 'tencentcloud',
    'value' => '',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1622202840,
    'updated_at' => 1622202840,
    'deleted_at' => 0,
  ),
  23 => 
  array (
    'id' => 24,
    'name' => '',
    'pid' => 23,
    'component' => '',
    'key' => 'tencentcloud.SecretId',
    'value' => '',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1622202840,
    'updated_at' => 1622202840,
    'deleted_at' => 0,
  ),
  24 => 
  array (
    'id' => 25,
    'name' => '',
    'pid' => 23,
    'component' => '',
    'key' => 'tencentcloud.SecretKey',
    'value' => '',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1622202840,
    'updated_at' => 1622202840,
    'deleted_at' => 0,
  ),
  25 => 
  array (
    'id' => 26,
    'name' => 'SaaS配置',
    'pid' => 0,
    'component' => 'saas',
    'key' => 'saas',
    'value' => '',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1622202840,
    'updated_at' => 1622202840,
    'deleted_at' => 0,
  ),
  26 => 
  array (
    'id' => 27,
    'name' => '',
    'pid' => 26,
    'component' => '',
    'key' => 'saas.sitetype',
    'value' => 'tenant',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1622202840,
    'updated_at' => 1637052506,
    'deleted_at' => 0,
  ),
  27 => 
  array (
    'id' => 28,
    'name' => '',
    'pid' => 26,
    'component' => '',
    'key' => 'appstoreserver.account',
    'value' => '',
    'status' => 1,
    'creator_id' => 0,
    'created_at' => 1622202840,
    'updated_at' => 1637050669,
    'deleted_at' => 0,
  ),
);

        foreach ($data as $item) {
            \catchAdmin\system\model\Config::create($item);
        }
    }
}