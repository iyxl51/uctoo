<?php

namespace catchAdmin\wechatopen\model;

use catcher\base\CatchModel as Model;
use catchAdmin\wechatopen\model\Applet;
use think\facade\Db;

class AdminApplet extends Model
{
    // 表名
    public $name = 'admin_applet';
    // 数据库字段映射
    public $field = array(
        'id',
        // appid
        'appid',
        // 状态
        'status',
        // 创建人ID
        'creator_id',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除
        'deleted_at',
    );

    public static function setApplet($creator_id , $appid)
    {
        Db::name('admin_applet')->where('creator_id','=',$creator_id)->delete();  //删除当前管理员选中的applet
        $res = self::create(['creator_id'=>$creator_id,'appid'=>$appid]);
        return $res;
    }

    public static function adminApplet($creator_id)
    {
        $adminApplet = self::where('creator_id','=',$creator_id)->find();  //返回当前管理员选中的applet
        $applet = Applet::where('appid',$adminApplet['appid'])->find();
        return $applet;
    }

    public static function getAppletConfig($creator_id)
    {
        $adminApplet = self::where('creator_id','=',$creator_id)->find();  //返回当前管理员选中的applet配置
        $applet = Applet::get(['appid'=>$adminApplet['appid']]);
        $adminAppletConfig = ['app_id'=>$applet['appid'],
            'secret' =>$applet['appsecret'],
            'token'=> $applet['token'],
            'aes_key' =>$applet['aeskey']
            ];

        return $adminAppletConfig;
    }
}