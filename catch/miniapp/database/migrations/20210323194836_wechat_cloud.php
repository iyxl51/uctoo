<?php

use think\migration\Migrator;
use think\migration\db\Column;
use Phinx\Db\Adapter\MysqlAdapter;

class WechatCloud extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('wechat_cloud', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '小程序云开发' ,'id' => 'id','signed' => true ,'primary_key' => ['id']]);
        $table->addColumn('appid', 'string', ['limit' => 255,'null' => false,'default' => '','signed' => true,'comment' => 'AppID',])
			->addColumn('env', 'string', ['limit' => 255,'null' => false,'default' => '','signed' => true,'comment' => '云环境id',])
			->addColumn('codesecret', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '代码秘钥',])
			->addColumn('info_list', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR,'null' => true,'signed' => true,'comment' => '环境信息',])
			->addColumn('status', 'string', ['limit' => 16,'null' => false,'default' => 1,'signed' => true,'comment' => '状态:-1=HALTED,0=UNAVAILABLE,1=NORMAL',])
			->addColumn('config', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR,'null' => true,'signed' => true,'comment' => '小程序配置json',])
			->addColumn('functions', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR,'null' => true,'signed' => true,'comment' => '云函数列表',])
			->addColumn('collections', 'text', ['limit' => MysqlAdapter::TEXT_REGULAR,'null' => true,'signed' => true,'comment' => '集合信息',])
			->addColumn('creator_id', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建人ID',])
			->addColumn('created_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建时间',])
			->addColumn('updated_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '更新时间',])
			->addColumn('deleted_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '软删除',])
            ->create();
    }
}
