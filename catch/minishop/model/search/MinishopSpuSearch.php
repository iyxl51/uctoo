<?php
namespace catchAdmin\minishop\model\search;

use catchAdmin\minishop\model\MinishopProductCategory;

trait MinishopSpuSearch
{
    public function searchTitleAttr($query, $value, $data)
    {
        return $query->whereLike('title', $value);
    }

    public function searchProductIdAttr($query, $value, $data)
    {
        return $query->whereLike('product_id', $value);
    }

    public function searchStatusAttr($query, $value, $data)
    {
        return $query->where($this->aliasField('status'), $value);
    }

    public function searchEditStatusAttr($query, $value, $data)
    {
        return $query->where($this->aliasField('edit_status'), $value);
    }

    /**
     * 查询分类下的商品
     *
     * @time 2020年11月04日
     * @param $query
     * @param $value
     * @param $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @return mixed
     */
    public function searchCatsAttr($query, $value, $data)
    {
        return $query->whereIn($this->aliasField('cats'), MinishopProductCategory::getChildrenCategoryIds($value));
    }

    /**
     * 查询分类下的商品
     *
     * @time 2020年11月04日
     * @param $query
     * @param $value
     * @param $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @return mixed
     */
    public function searchCategoryIdAttr($query, $value, $data)
    {
        return $query->whereIn($this->aliasField('cats'), MinishopProductCategory::getChildrenCategoryIds($value));
    }
}
