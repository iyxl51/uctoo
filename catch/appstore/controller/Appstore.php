<?php

namespace catchAdmin\appstore\controller;

use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\appstore\model\Appstore as AppstoreModel;
use catcher\Utils;
use uctoo\uctoocloud\client\Http;

class Appstore extends CatchController
{
    protected $AppstoreModel;
    
    public function __construct(AppstoreModel $AppstoreModel)
    {
        $this->AppstoreModel = $AppstoreModel;
    }
    
    /**
     * 列表
     * @time 2021年05月13日 19:02
     * @param Request $request 
     */
    public function index(Request $request)
    {
        $result = null;

        $host = trim(Utils::config('appstoreserver.host'));//获取管理后台配置的微信第三方平台地址 http://serv.uctoo.com
        $data = input('param.');
        $response = Http:: withHost($host)   //指定第三方平台
        ->withVerify(false)    //无需SSL验证
        ->product('')    //如果指定产品 则必填参数
        ->get('/appstoreserver', $data);  //当前输入的应用市场帐号
        $res = json_decode($response->body(),true);
        $result = $res['data']['data'];
        if($res['code'] == 10000){
            return CatchResponse::success($result);
        }
    }
    
    /**
     * 保存信息
     * @time 2021年05月13日 19:02
     * @param Request $request 
     */
    public function save(Request $request) : \think\Response
    {
        return CatchResponse::success($this->AppstoreModel->storeBy($request->post()));
    }
    
    /**
     * 读取
     * @time 2021年05月13日 19:02
     * @param $id 
     */
    public function read($id) : \think\Response
    {
        return CatchResponse::success($this->AppstoreModel->findBy($id));
    }
    
    /**
     * 更新
     * @time 2021年05月13日 19:02
     * @param Request $request 
     * @param $id
     */
    public function update(Request $request, $id) : \think\Response
    {
        return CatchResponse::success($this->AppstoreModel->updateBy($id, $request->post()));
    }
    
    /**
     * 删除
     * @time 2021年05月13日 19:02
     * @param $id
     */
    public function delete($id) : \think\Response
    {
        return CatchResponse::success($this->AppstoreModel->deleteBy($id));
    }

    /**
     * 购买
     * @time 2021年05月13日 19:02
     * @param $id
     */
    public function orderAdd($id)
    {
        $param = input('param.');
        $host = trim(Utils::config('appstoreserver.host'));//获取管理后台配置的微信第三方平台地址 http://serv.uctoo.com
        $response = Http:: withHost($host)   //指定第三方平台
        ->withHeaders(['Authorization'=>$param['appstore_user_token']])  //采用服务端帐号认证
        ->withVerify(false)    //无需SSL验证
        ->product('')    //如果指定产品 则必填参数
        ->post('/appstoreorder', $param);  //当前输入的应用市场帐号
        $res = json_decode($response->body(),true);
        if($res['code'] == 10000){

        }
        return $res;
    }
}