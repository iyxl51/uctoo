<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2021 http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: JaguarJack [ njphper@gmail.com ]
// +----------------------------------------------------------------------

// test路由
$router->group(function () use ($router){
    # test
    $router->resource('test', '\catchAdmin\test\controller\Test');
	// demo路由
	$router->resource('demo', '\catchAdmin\test\controller\Demo');

})->middleware('auth');

