<?php

namespace catchAdmin\appstore\model;

use catcher\base\CatchModel as Model;
/**
 *
 * @property int $id
 * @property string $email
 * @property string $avatar
 * @property string $remember_token
 * @property string $appid
 * @property string $env
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 * @property int $creator_id
 */
class AppstoreUsers extends Model
{
    // 表名
    public $name = 'appstore_users';
    // 数据库字段映射
    public $field = array(
        'id',
        // 邮箱 登录
        'email',
        // 用户头像
        'avatar',
        // 用户token
        'remember_token',
        // appid
        'appid',
        // 云环境ID
        'env',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除字段
        'deleted_at',
        // 创建人ID
        'creator_id',
    );

    /**
     * 查询列表
     *
     * @time 2020年04月28日
     * @return mixed
     */
    public function getList()
    {
        // 不分页
        if (property_exists($this, 'paginate') && $this->paginate === false) {
            return $this->dataRange()->catchSearch()
                ->field('*')
                ->catchOrder()
                ->creator()
                ->select();
        }

        // 分页列表
        return $this->dataRange()->catchSearch()
            ->field('*')
            ->catchOrder()
            ->creator()
            ->paginate();
    }
}