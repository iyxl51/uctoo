<?php

namespace catchAdmin\appstore\controller;

use catchAdmin\wechatopen\model\AdminApplet;
use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\appstore\model\AppstoreUsers as AppstoreUsersModel;
use catchAdmin\permissions\model\DataRangScopeTrait;
use catcher\Code;
use catcher\Utils;
use uctoo\uctoocloud\client\Http;

class AppstoreUsers extends CatchController
{
    use DataRangScopeTrait;
    protected $AppstoreUsersModel;
    
    public function __construct(AppstoreUsersModel $AppstoreUsersModel)
    {
        $this->AppstoreUsersModel = $AppstoreUsersModel;
    }
    
    /**
     * 列表
     * @time 2021年08月02日 19:20
     * @param Request $request 
     */
    public function index(Request $request) : \think\Response
    {
        return CatchResponse::paginate($this->AppstoreUsersModel->getList());
    }
    
    /**
     * 保存信息
     * @time 2021年08月02日 19:20
     * @param Request $request 
     */
    public function save(Request $request) : \think\Response
    {
        return CatchResponse::success($this->AppstoreUsersModel->storeBy($request->post()));
    }
    
    /**
     * 读取
     * @time 2021年08月02日 19:20
     * @param $id 
     */
    public function read($id) : \think\Response
    {
        return CatchResponse::success($this->AppstoreUsersModel->findBy($id));
    }
    
    /**
     * 更新
     * @time 2021年08月02日 19:20
     * @param Request $request 
     * @param $id
     */
    public function update(Request $request, $id) : \think\Response
    {
        return CatchResponse::success($this->AppstoreUsersModel->updateBy($id, $request->post()));
    }
    
    /**
     * 删除
     * @time 2021年08月02日 19:20
     * @param $id
     */
    public function delete($id) : \think\Response
    {
        return CatchResponse::success($this->AppstoreUsersModel->deleteBy($id));
    }

    /**
     * 应用市场登录
     * @time 2021年08月02日 19:20
     * @param
     */
    public function appstoreLogin(Request $request) : \think\Response
    {
        $result = null;

        $host = trim(Utils::config('appstoreserver.host'));//获取管理后台配置的微信第三方平台地址 http://serv.uctoo.com
        $account = input('email');
        $password = input('password');
        $response = Http:: withHost($host)   //指定第三方平台
        ->withVerify(false)    //无需SSL验证
        ->product('')    //如果指定产品 则必填参数
        ->post('/login', ['email' => $account,'password'=>$password]);  //当前输入的应用市场帐号
        $res = json_decode($response->body(),true);
        $remember_token =  $res['data']['token'];
        trace($res,'debug');

        if($res['code'] == 10000){
            trace('10000','debug');
            trace($res['data']['token'],'debug');
            $response = Http:: withHost($host)   //指定第三方平台
            ->withHeaders(['Authorization'=>'Bearer'.$remember_token])  //采用服务端帐号认证
            ->withVerify(false)    //无需SSL验证
            ->product('')    //如果指定产品 则必填参数
            ->get('/users', ['email' => $account]);  //当前输入的应用市场帐号
            $res = json_decode($response->body(),true);
            trace($res,'debug');
            $data = [];
            if($res['code'] == '10000'){

                $data['email'] = $res['data'][0]['email'];
                $data['avatar'] = $res['data'][0]['avatar'];
                $data['remember_token'] = $remember_token;
                $data['creator_id'] = $res['data'][0]['id'];

                $appstore_user = $this->AppstoreUsersModel->where(['email'=>$account])->find();
                if($appstore_user){ //本地已保存了远程用户，只更新token
                    $this->AppstoreUsersModel->updateBy($appstore_user['id'],['remember_token'=>$remember_token]);
                }else{ //本地未保存远程用户，存下

                    $this->AppstoreUsersModel->storeBy($data);
                }
            }
            return CatchResponse::success($data,'登录成功');
        }
        return CatchResponse::fail('登录失败',Code::LOGIN_FAILED);
    }
}