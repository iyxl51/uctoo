<?php

namespace catchAdmin\wechatopen\model;

use catchAdmin\permissions\model\DataRangScopeTrait;
use catchAdmin\permissions\model\Users;
use catchAdmin\wechatopen\model\search\AppletSearchTrait;
use catcher\base\CatchModel as Model;

class Applet extends Model
{
    use AppletSearchTrait;
    use DataRangScopeTrait;
    // 表名
    public $name = 'wechatopen_applet';
    // 数据库字段映射
    public $field = array(
        'id',
        // 应用名称
        'name',
        // 应用类型
        'typedata',
        // Token
        'token',
        // appid
        'appid',
        // appsecret
        'appsecret',
        // EncodingAESKey
        'aeskey',
        // 微信支付商户号
        'mchid',
        // 商户支付密钥
        'mchkey',
        // 商户API证书cert
        'mch_api_cert',
        // 商户API证书key
        'mch_api_key',
        // 微信支付异步通知url
        'notify_url',
        // 主体名称
        'principal',
        // 原始ID
        'original',
        // 微信号
        'wechat',
        // 头像
        'headface_image',
        // 二维码图片
        'qrcode_image',
        // 账号介绍
        'signature',
        // 省市
        'city',
        // 状态
        'status',
        // 权重
        'weigh',
        // 授权方公众号类型
        'service_type_info',
        // 授权方认证类型
        'verify_type_info',
        // 用以了解公众号功能的开通状况
        'business_info',
        // 第三方平台授权token
        'authorizer_access_token',
        // 授权token过期时间
        'access_token_overtime',
        // 授权刷新token
        'authorizer_refresh_token',
        // 小程序信息
        'miniprograminfo',
        // 公众号授权给开发者的权限集列表
        'func_info',
        // jsapi ticket
        'ticket',
        // jsapi ticket 过期时间
        'ticket_overtime',
        // 创建人ID
        'creator_id',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除
        'deleted_at',
    );


    const STATUS_UNAUTHORIZED = 0;
    const STATUS_AUTHORIZED = 1;

    const TYPE_MINI_PROGRAM = 1;
    const TYPE_OFFICIAL_ACCOUNT = 2;

    // 普通授权
    const AUTHORIZE_TYPE_NORMAL = 1;
    // 通过快速注册授权
    const AUTHORIZE_TYPE_REGISTER = 2;

    const AUDIT_STATUS_SUCCESS = 0;
    const AUDIT_STATUS_FAIL = 1;
    const AUDIT_STATUS_WAIT = 2;
    const AUDIT_STATUS_REVOKE = 3;
    const AUDIT_STATUS_DELAY = 4;
    
    public static function getAppletConfig($appid){
        $applet = self::where('appid',$appid)->find();
        $config = ['app_id'=>$applet['appid'], 'secret' =>$applet['appsecret'] , 'token'=> $applet['token'], 'aes_key' =>$applet['aeskey']];
        $wechat_default = config('wechat.default') ? config('wechat.default') : [];
        $oauth_config = ['oauth' => [
            'scopes'   => ['snsapi_userinfo'],
            'callback' => 'admin/index/wxlogin_callback',
        ]];  //默认的公众号oauth配置，可在具体应用点赋值其他配置修改
        $config = array_merge($config,$wechat_default,$oauth_config);
        return $config;

    }

    /**
     * get list
     *
     * @time 2020年04月28日
     * @param $params
     * @throws \think\db\exception\DbException
     * @return void
     */
    public function getList()
    {
        return $this->dataRange()->field([$this->aliasField('*')])
            ->catchJoin(Users::class, 'id', 'creator_id', ['username as creator'])
            ->catchSearch()
            ->order($this->aliasField('id'), 'desc')
            ->paginate();
    }

}