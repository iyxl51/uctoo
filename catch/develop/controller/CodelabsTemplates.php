<?php

namespace catchAdmin\develop\controller;

use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\develop\model\CodelabsTemplates as CodelabsTemplatesModel;
use catcher\Utils;
use think\facade\Log;
use think\Response;
use uctoo\uctoocloud\client\Http;

class CodelabsTemplates extends CatchController
{
    
    protected $codelabsTemplatesModel;
    
    /**
     *
     * @time 2021/11/03 16:06
     * @param CodelabsTemplatesModel $codelabsTemplatesModel
     * @return mixed
     */
    public function __construct(CodelabsTemplatesModel $codelabsTemplatesModel)
    {
        $this->codelabsTemplatesModel = $codelabsTemplatesModel;
    }
    
    /**
     *
     * @time 2021/11/03 16:06
     * @return Response
     */
    public function index()
    {

        $param = input('param.');
        $host = trim(Utils::config('appstoreserver.host'));//获取管理后台配置的应用市场服务地址 http://serv.uctoo.com
        $sitetype = trim(Utils::config('saas.sitetype')); //站点类型
        if($sitetype == 'tenant'){ //从应用市场获取模板
            if($param['appstore_user_token'] != 'Bearerundefined'){
                $response = Http:: withHost($host)   //指定SaaS平台
                ->withHeaders(['Authorization'=>$param['appstore_user_token']])  //采用服务端帐号认证
                ->withVerify(false)    //无需SSL验证
                ->product('')    //如果指定产品 则必填参数
                ->get('/codelabsTemplates', $param);  //当前输入的应用市场帐号
                $res = json_decode($response->body(),true);
                if($res['code'] == 10000){
                    return $res;
                }else{
                    return CatchResponse::fail('获取应用市场模板文件错误');
                }
            }else{
                return CatchResponse::fail('请先登录应用市场');
            }
        }else{
            $res = CatchResponse::paginate($this->codelabsTemplatesModel->getList());
            return $res;
        }
    }
    
    /**
     *
     * @time 2021/11/03 16:06
     * @param Request $request
     * @return Response
     */
    public function save(Request $request) : Response
    {
        return CatchResponse::success($this->codelabsTemplatesModel->storeBy($request->post()));
    }
    
    /**
     *
     * @time 2021/11/03 16:06
     * @param $id
     * @return Response
     */
    public function read($id) : Response
    {
        return CatchResponse::success($this->codelabsTemplatesModel->findBy($id));
    }
    
    /**
     *
     * @time 2021/11/03 16:06
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request) : Response
    {
        return CatchResponse::success($this->codelabsTemplatesModel->updateBy($id, $request->post()));
    }
    
    /**
     *
     * @time 2021/11/03 16:06
     * @param $id
     * @return Response
     */
    public function delete($id) : Response
    {
        return CatchResponse::success($this->codelabsTemplatesModel->deleteBy($id));
    }
}