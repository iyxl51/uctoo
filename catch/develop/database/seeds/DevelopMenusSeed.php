<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~{$year} http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: JaguarJack [ njphper@gmail.com ]
// +----------------------------------------------------------------------

use think\migration\Seeder;

class DevelopMenusSeed extends Seeder
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        \catcher\Utils::importTreeData($this->getPermissions(), 'permissions', 'parent_id');
    }

    protected function getPermissions()
    {
       return array (
  0 => 
  array (
    'id' => 269,
    'permission_name' => 'develop',
    'parent_id' => 0,
    'level' => '1',
    'route' => '/develop',
    'icon' => 'el-icon-star-off',
    'module' => 'develop',
    'creator_id' => 1,
    'permission_mark' => 'develop',
    'component' => 'layout',
    'redirect' => '',
    'keepalive' => 1,
    'type' => 1,
    'hidden' => 1,
    'sort' => 0,
    'created_at' => 1635815695,
    'updated_at' => 1635824271,
    'deleted_at' => 0,
  ),
  1 => 
  array (
    'id' => 270,
    'permission_name' => 'codelabsCURD',
    'parent_id' => 269,
    'level' => '1-269',
    'route' => '/develop/codelabs/curd',
    'icon' => '',
    'module' => 'develop',
    'creator_id' => 1,
    'permission_mark' => 'codelabs',
    'component' => 'develop_codelabs',
    'redirect' => '',
    'keepalive' => 1,
    'type' => 1,
    'hidden' => 2,
    'sort' => 0,
    'created_at' => 1635815696,
    'updated_at' => 1636110961,
    'deleted_at' => 0,
  ),
  2 => 
  array (
    'id' => 271,
    'permission_name' => '列表',
    'parent_id' => 270,
    'level' => '1-269-270',
    'route' => '',
    'icon' => '',
    'module' => 'develop',
    'creator_id' => 1,
    'permission_mark' => 'codelabs@index',
    'component' => '',
    'redirect' => '',
    'keepalive' => 1,
    'type' => 2,
    'hidden' => 1,
    'sort' => 1,
    'created_at' => 1635815698,
    'updated_at' => 1636110961,
    'deleted_at' => 0,
  ),
  3 => 
  array (
    'id' => 272,
    'permission_name' => '保存',
    'parent_id' => 270,
    'level' => '1-269-270',
    'route' => '',
    'icon' => '',
    'module' => 'develop',
    'creator_id' => 1,
    'permission_mark' => 'codelabs@save',
    'component' => '',
    'redirect' => '',
    'keepalive' => 1,
    'type' => 2,
    'hidden' => 1,
    'sort' => 1,
    'created_at' => 1635815699,
    'updated_at' => 1636110961,
    'deleted_at' => 0,
  ),
  4 => 
  array (
    'id' => 273,
    'permission_name' => '读取',
    'parent_id' => 270,
    'level' => '1-269-270',
    'route' => '',
    'icon' => '',
    'module' => 'develop',
    'creator_id' => 1,
    'permission_mark' => 'codelabs@read',
    'component' => '',
    'redirect' => '',
    'keepalive' => 1,
    'type' => 2,
    'hidden' => 1,
    'sort' => 1,
    'created_at' => 1635815700,
    'updated_at' => 1636110961,
    'deleted_at' => 0,
  ),
  5 => 
  array (
    'id' => 274,
    'permission_name' => '更新',
    'parent_id' => 270,
    'level' => '1-269-270',
    'route' => '',
    'icon' => '',
    'module' => 'develop',
    'creator_id' => 1,
    'permission_mark' => 'codelabs@update',
    'component' => '',
    'redirect' => '',
    'keepalive' => 1,
    'type' => 2,
    'hidden' => 1,
    'sort' => 1,
    'created_at' => 1635815702,
    'updated_at' => 1636110961,
    'deleted_at' => 0,
  ),
  6 => 
  array (
    'id' => 275,
    'permission_name' => '删除',
    'parent_id' => 270,
    'level' => '1-269-270',
    'route' => '',
    'icon' => '',
    'module' => 'develop',
    'creator_id' => 1,
    'permission_mark' => 'codelabs@delete',
    'component' => '',
    'redirect' => '',
    'keepalive' => 1,
    'type' => 2,
    'hidden' => 1,
    'sort' => 1,
    'created_at' => 1635815703,
    'updated_at' => 1636110961,
    'deleted_at' => 0,
  ),
  7 => 
  array (
    'id' => 276,
    'permission_name' => 'codelabs',
    'parent_id' => 269,
    'level' => '1-269',
    'route' => '/develop/codelabs',
    'icon' => 'el-icon-magic-stick',
    'module' => 'develop',
    'creator_id' => 1,
    'permission_mark' => 'codelabsindex',
    'component' => 'develop_codelabsindex',
    'redirect' => '',
    'keepalive' => 1,
    'type' => 1,
    'hidden' => 1,
    'sort' => 1,
    'created_at' => 1635823981,
    'updated_at' => 1635853313,
    'deleted_at' => 0,
  ),
  8 => 
  array (
    'id' => 277,
    'permission_name' => 'templatesCURD',
    'parent_id' => 269,
    'level' => '1-269',
    'route' => '/develop/codelabs/templatescurd',
    'icon' => 'el-icon-document-copy',
    'module' => 'develop',
    'creator_id' => 1,
    'permission_mark' => 'codelabsTemp',
    'component' => 'develop_codelabsTemplates',
    'redirect' => '',
    'keepalive' => 1,
    'type' => 1,
    'hidden' => 1,
    'sort' => 1,
    'created_at' => 1635928251,
    'updated_at' => 1635928251,
    'deleted_at' => 0,
  ),
  9 => 
  array (
    'id' => 278,
    'permission_name' => 'algorithmCURD',
    'parent_id' => 269,
    'level' => '1-269',
    'route' => '/develop/codelabs/algorithmcurd',
    'icon' => '',
    'module' => 'develop',
    'creator_id' => 1,
    'permission_mark' => 'codelabs_algorithm',
    'component' => 'develop_codelabs_algorithm',
    'redirect' => '',
    'keepalive' => 1,
    'type' => 1,
    'hidden' => 1,
    'sort' => 1,
    'created_at' => 1635996570,
    'updated_at' => 1635996603,
    'deleted_at' => 0,
  ),
  10 => 
  array (
    'id' => 280,
    'permission_name' => '代码生成',
    'parent_id' => 270,
    'level' => '1-269-270',
    'route' => '',
    'icon' => '',
    'module' => 'develop',
    'creator_id' => 1,
    'permission_mark' => 'codelabs@generate',
    'component' => '',
    'redirect' => '',
    'keepalive' => 1,
    'type' => 2,
    'hidden' => 1,
    'sort' => 1,
    'created_at' => 1636109680,
    'updated_at' => 1636110961,
    'deleted_at' => 0,
  ),
);
    }
}
