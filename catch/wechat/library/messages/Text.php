<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2020 http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: JaguarJack [ njphper@gmail.com ]
// +----------------------------------------------------------------------
namespace catchAdmin\wechat\library\messages;

use think\facade\Log;
use catchAdmin\wechat\model\WechatReply;

class Text extends Message
{
    public function reply()
    {
        // TODO: Implement reply() method.
        Log::write($this->message,'debug');

        $reply = new WechatReply;
        //关键词匹配
        $replyMessage = $reply->where('keyword','=',$this->message['Content'])->where('rule_type','=',1)->column('content');
        if($replyMessage){ //匹配到了关键词
            return $replyMessage[0];
        }else{  //默认回复
            $replyMessage = $reply->where('rule_type','=',3)->column('content');
            if($replyMessage){
                return $replyMessage[0];
            }else{
                return 'success';
            }
        }
    }
}