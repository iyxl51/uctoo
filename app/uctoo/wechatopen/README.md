wechatopen 模块是一个用于对接微信生态所有API的标准微信第三方平台前置服务器。

# 概述

  本模块的设计目标是提供开发人员、产品人员、运维人员等相关角色，可以一站式对接微信生态API，并且所有对接方式和微信官方文档保持一致，开发人员可以无门槛的快速进行微信应用的开发。
  
  
最近看到微信官方发布了一个微信云托管的平台。https://cloud.weixin.qq.com

![图片](https://gitee.com/UCT_admin/materials/raw/master/uctoo_wechatopen/cloud%20function.png)

*图1.微信云托管平台云调用功能*

  

其中有一个云调用的功能，感觉和我们UCToo产品中的wechatopen模块有相同的目标和作用。应该不存在谁抄谁的事情，应该都是为了降低开发者调用微信相关接口的门槛，平行开发出的相似功能模块。

  

7月1日发布API客户端SDK时已经提到过了wechatopen模块，wechatopen模块最早应该是在2021年3、4月份的时候就已经作为一个内置模块提交到了[https://gitee.com/uctoo/uctoo](https://gitee.com/uctoo/uctoo)项目代码库。但是一直没有正式发布wechatopen模块，主要是目前其实只规范化实现了大致5%的微信接口，主要是以我们目前先用到了的微信小商店开放接口进行了开发规范的示例，其他微信接口都还没有具体进行规范化实现。Wechatopen模块的开发规范已经确定了，后续更多接口的实现都只是工作量的事情，没有什么复杂度，待我们用到其他微信接口的时候或者由其他开源开发者补充就行了。今天正式发布一下wechatopen模块，给开发者提供一个明晰的示例和开发规范参考。

  

Wechatopen模块是采用easywechat SDK进行的一个开放平台开发模式的二次封装。很多PHP项目涉及到微信开发的时候，都用了easywechat SDK，但是easywechat SDK主要是提供给后台开发调用的，也主要是PHP开发者使用，而且easywechat SDK由于作者的设计理念参考了Laravel框架，感觉有些过封装，就是PHP开发者使用起来也不是很简洁直观。SDK设计的初衷就是简化开发者的使用门槛，当使用一个SDK的时候，还要先学习这个SDK的文档，就感觉有点复杂了。因此UCToo产品，规划采用微信第三方平台的开发方式，把微信相关接口都SaaS化，提供出和微信官方文档完全相同的调用地址（只需切换服务商域名）和相同的调用参数及返回值。真正简化开发者对接微信接口的门槛，并且Wechatopen模块已经以第三方平台处理了access\_token等前置调用参数，规范化实现后的微信接口可以提供给PC、APP、小程序等各种客户端调用，提高微信相关应用开发的效率。同时，结合uctoo-api-client、apitester模块等使用，可以让微信相关开发更直观和可视化，可以解耦所有和微信相关的功能开发。因此将wechatopen模块称为微信开放平台前置服务器是合理恰当的。鉴于微信生态的广泛覆盖，几乎所有SaaS产品都或多或少有需要和微信对接的功能和场景，因此微信开放平台前置服务器应该会是微信生态最常使用的生态基础设施之一。

  

开发者在使用www.uctoo.com 官网的微信开放平台前置服务器前，需要先在www.uctoo.com 注册用户帐号，并且将需要调用微信接口的小程序、公众号、微信小商店等帐号，通过扫码授权到www.uctoo.com ，之后就可以在API管理模块，查看www.uctoo.com 已经实现的微信开放平台接口，运行API测试示例。

  

UCToo的wechatopen微信开放平台前置服务器与微信官方的云调用也有比较显著的差异。云调用依然没有提供客户端的SDK规范，也就是之前说过的多数云服务商只管云不管端的缺失。UCToo提供了客户端开发SDK，uctoo-api-client 开源SDK，通过开源项目约定了客户端开发规范，有望实现基于UCToo开发的应用具有更高的可复用性。同时，Wechatopen模块除了可以作为www.uctoo.com 官网提供的一项SaaS服务外，也支持在大客户、产业互联网等项目中作为独立第三方平台前置服务器部署发布。可以实现客户项目的APP、小程序、官网等多应用采用unionid机制实现用户身份统一帐号，支持依赖微信第三方平台开发模式的微信小商店、微信批量云开发等高级特性的产品定制开发。Wechatopen模块也是开源的，未实现的接口和功能，开发者可以自主添加。Wechatopen模块的开源地址：

[https://gitee.com/uctoo/uctoo/tree/master/app/uctoo/wechatopen](https://gitee.com/uctoo/uctoo/tree/master/app/uctoo/wechatopen)

  

开发者扩展wechatopen模块时，请参考如下开发规范：

1\. 在路由文件route.php中定义与微信官方接口完全一致的API地址。只在每个API地址前增加api/wechatopen产品模块路由前缀。所有接口约定采用POST请求。如下示例代码：

~~~
    // wechat路由  微信小商店 标准版交易组件
    $router->post('api/wechatopen/product/category/get', '\app\uctoo\wechatopen\api\Wechatopen@productCategoryGet');
    $router->post('api/wechatopen/product/brand/get', '\app\uctoo\wechatopen\api\Wechatopen@productBrandGet');
    $router->post('api/wechatopen/product/delivery/get_freight_template', '\app\uctoo\wechatopen\api\Wechatopen@productDeliveryGet_freight_template');
    $router->post('api/wechatopen/product/store/get_shopcat', '\app\uctoo\wechatopen\api\Wechatopen@productStoreGet_shopcat');
    $router->post('api/wechatopen/product/spu/add', '\app\uctoo\wechatopen\api\Wechatopen@productSpuAdd');
    $router->post('api/wechatopen/product/spu/del', '\app\uctoo\wechatopen\api\Wechatopen@productSpuDel');

~~~

2\. 在api/wechatopen.php文件中，对接实现每个微信接口时，保持与微信官方文档完全相同的请求参数和返回值。

  

3\. 在www.uctoo.com API管理模块新建API测试用例，提供开发者实测接口的示例。