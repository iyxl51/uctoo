<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2021 http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: JaguarJack [ njphper@gmail.com ]
// +----------------------------------------------------------------------

// appstore路由
$router->group(function () use ($router){
    $router->resource('appstore', '\catchAdmin\appstore\controller\Appstore');
    $router->post('appstore/order/add/<id>', '\catchAdmin\appstore\controller\Appstore@orderAdd');
	// appstoreUsers路由
	$router->resource('appstoreUsers', '\catchAdmin\appstore\controller\AppstoreUsers');
    $router->post('appstore/login', '\catchAdmin\appstore\controller\AppstoreUsers@appstoreLogin');
    // AppstoreProductCategory路由
    $router->resource('appstore/product/category', '\catchAdmin\appstore\controller\AppstoreProductCategory');
});
