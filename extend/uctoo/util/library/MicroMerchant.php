<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Patrick <contact@uctoo.com>
// +----------------------------------------------------------------------

namespace uctoo\util\library;


use EasyWeChat\Factory;
use think\facade\Config;

class MicroMerchant
{
    public static function instance(string $subMchId = null){
        $app = Factory::microMerchant(Config::get('micro_merchant'));//TODO:从管理后台配置获取
        !is_null($subMchId) && $app->setSubMchId($subMchId);
        $app->rebind('cache',app('cache'));
        return $app;
    }
}