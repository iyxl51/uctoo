<?php

use think\migration\Migrator;
use think\migration\db\Column;
use Phinx\Db\Adapter\MysqlAdapter;

class Wechatopen extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('wechatopen', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '微信第三方平台信息表' ,'id' => 'id','signed' => true ,'primary_key' => ['id']]);
        $table->addColumn('appid', 'string', ['limit' => 255,'null' => false,'default' => '','signed' => true,'comment' => 'appid',])
			->addColumn('appsecret', 'string', ['limit' => 255,'null' => false,'default' => '','signed' => true,'comment' => 'appsecret',])
			->addColumn('encodingAesKey', 'string', ['limit' => 43,'null' => false,'default' => '','signed' => true,'comment' => 'encodingAesKey',])
			->addColumn('component_verify_ticket', 'string', ['limit' => 255,'null' => false,'default' => '','signed' => true,'comment' => 'component_verify_ticket',])
			->addColumn('component_access_token', 'string', ['limit' => 255,'null' => false,'default' => '','signed' => true,'comment' => 'component_access_token',])
			->addColumn('token_overtime', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => true,'comment' => 'token过期时间',])
			->addColumn('pre_auth_code', 'string', ['limit' => 255,'null' => false,'default' => '','signed' => true,'comment' => '预授权码',])
			->addColumn('pre_code_overtime', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => true,'comment' => '预授权过期时间',])
			->addColumn('status', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => true,'comment' => '状态',])
			->addColumn('token', 'string', ['limit' => 255,'null' => false,'default' => '','signed' => true,'comment' => 'token',])
			->addColumn('creator_id', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建人ID',])
			->addColumn('created_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建时间',])
			->addColumn('updated_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '更新时间',])
			->addColumn('deleted_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '软删除',])
            ->create();
    }
}
