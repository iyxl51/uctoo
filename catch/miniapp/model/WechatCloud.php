<?php

namespace catchAdmin\miniapp\model;

use catcher\base\CatchModel as Model;

class WechatCloud extends Model
{
    // 表名
    public $name = 'wechat_cloud';
    // 数据库字段映射
    public $field = array(
        'id',
        // AppID
        'appid',
        // 云环境id
        'env',
        // 代码秘钥
        'codesecret',
        // 环境信息
        'info_list',
        // 状态:-1=HALTED,0=UNAVAILABLE,1=NORMAL
        'status',
        // 小程序配置json
        'config',
        // 云函数列表
        'functions',
        // 集合信息
        'collections',
        // 创建人ID
        'creator_id',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除
        'deleted_at',
    );

    /**
     * appid查询
     *
     * @time 2020年06月17日
     * @param $query
     * @param $value
     * @param $data
     * @return mixed
     */
    public function searchAppidAttr($query, $value, $data)
    {
        return $query->whereLike('appid', $value);
    }
}