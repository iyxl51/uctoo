<?php
declare (strict_types = 1);

namespace uctoo\event;

use think\exception\HttpResponseException;
use think\Response;

class InvokeCloudFunctionBefore
{
    /**
     * @var 原函数名
     */
    public $func_name;
    /**
     * @var 云函数参数
     */
    public $func_params;
    /**
     * @var 拓展内容
     */
    public $extends;

    public function __construct($func_name,array &$func_params,$extends = null)
    {
        $this->func_name = $func_name;
        $this->func_params = &$func_params;
        $this->extends = $extends;
    }

    public function setFuncParams($key,$value){
        $this->func_params[$key] = $value;
    }

    public function returnResponse($msg, $data = null, $code = 0, $type = 'json', array $header = [])
    {
        $result = [
            'code' => $code,
            'msg'  => $msg,
            'data' => $data,
        ];
        $response = Response::create($result, $type, 200)->header($header);
        throw new HttpResponseException($response);
    }

}
