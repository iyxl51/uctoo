<?php

namespace catchAdmin\develop\model;

use catcher\base\CatchModel as Model;
/**
 *
 * @property int $id
 * @property string $command_name
 * @property string $algorithm_code
 * @property string $filename
 * @property string $filepath
 * @property string $template_url
 * @property int $opensource
 * @property string $author
 * @property string $author_url
 * @property int $user_id
 * @property string $tags
 * @property string $docs
 * @property string $images
 * @property string $description
 * @property int $price
 * @property int $status
 * @property string $arguments
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 * @property int $creator_id
 */
class CodelabsAlgorithm extends Model
{
    
    public $field = [
        //
        'id',
        // 命令名称
        'command_name',
        // 算法代码
        'algorithm_code',
        // 算法文件名
        'filename',
        // 算法路径
        'filepath',
        // 算法url地址
        'template_url',
        // 是否开源
        'opensource',
        // 作者
        'author',
        // 作者url
        'author_url',
        // 关联users表id
        'user_id',
        // 算法标签
        'tags',
        // 算法文档
        'docs',
        // 算法图片
        'images',
        // 算法描述
        'description',
        // 算法标价
        'price',
        // 算法状态
        'status',
        // 命令参数
        'arguments',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除字段
        'deleted_at',
        // 创建人ID
        'creator_id',
    ];
    
    public $name = 'codelabs_algorithm';
}