<?php
declare (strict_types = 1);

namespace uctoo\validate;

use think\Validate;

class Component extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
	    'name' => ['require'],
	    'code' => ['require'],
	    'code_type' => ['require','in:1,2,3'],
	    'legal_persona_wechat' => ['require'],
	    'legal_persona_name' => ['require'],
	    'component_phone' => ['require'],
    ];

    protected $message = [
        'name.require' => '企业名必须',
        'code.require' => '企业代码',
        'code_type.require' => '企业代码类型必须',
        'code_type.in' => '企业代码类型错误',
        'legal_persona_wechat.require' => '法人微信必须',
        'legal_persona_name.require' => '法人姓名必须',
        'component_phone.require' => '第三方联系电话必须',
    ];
    
    protected $scene = [
      'query' => ['name','code','legal_persona_wechat','legal_persona_name']
    ];
}
