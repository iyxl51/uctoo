<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Patrick <contact@uctoo.com>
// +----------------------------------------------------------------------

namespace uctoo\util\EventHandler\openPlatform;


use catchAdmin\wechat\library\messages\Factory;
use catchAdmin\wechatopen\model\Applet;
use EasyWeChat\Kernel\Contracts\EventHandlerInterface;
use EasyWeChat\OpenPlatform\Application;
use think\facade\Cache;
use uctoo\ThinkEasyWeChat\Facade;
use uctoo\util\library\Mpopen;

class EventMessageHandler implements EventHandlerInterface
{

    protected $appid;

    public function __construct($appid)
    {
        $this->appid = $appid;
    }

    public function handle($payload = null)
    {
        if ($res = Factory::make($payload)->reply()) {
            return $res;
        }
       /* switch ($payload['MsgType']) {
            case 'event':
                //接收事件推送: https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140454
                switch ($payload['Event']) {
                    case 'subscribe':  //关注事件, 扫描带参数二维码事件(用户未关注时，进行关注后的事件推送)
                        return "欢迎关注";
                        break;
                    case 'unsubscribe':  //取消关注事件
                        break;
                    case 'SCAN':  //扫描带参数二维码事件(用户已关注时的事件推送)
                        return "欢迎关注";
                        break;
                    case 'LOCATION':  //上报地理位置事件
                        //return "经度: " . $payload['Longitude'] . "\n纬度: " . $payload['Latitude'] . "\n精度: " . $payload['Precision'];
                        break;
                    case 'CLICK':  //自定义菜单事件(点击菜单拉取消息时的事件推送)
                        //return "事件KEY值: " . $payload['EventKey'];
                        break;
                    case 'VIEW':  //自定义菜单事件(点击菜单拉取消息时的事件推送)
                        //return "跳转URL: " . $payload['EventKey'];
                        break;
                    case 'ShakearoundUserShake':
                        //摇一摇事件通知: https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1443448066
                       // return 'ChosenBeacon\n' . 'Uuid: ' . $payload['ChosenBeacon']['Uuid'] . 'Major: ' . $payload['ChosenBeacon']['Major'] . 'Minor: ' . $payload['ChosenBeacon']['Minor'] . 'Distance: ' . $payload['ChosenBeacon']['Distance'];
                        break;
                    case 'wxa_nickname_audit':
                        // 名称审核
                        // ret nickname  reason
                        break;
                    case 'weapp_audit_success':
                        // 收到代码审核成功消息后自动自动发布小程序
                        $applet = Applet::where('appid',$this->appid)->field('appid,refresh_token,current_version,audit_version,code_tpl_id,audit_tpl_id')->find();
                        Applet::update([
                            'current_version' => $applet['audit_version'],
                            'code_tpl_id' => $applet['audit_tpl_id'],
                            'audit_time' => $payload['SuccTime'],
                            'audit_status' => Applet::AUDIT_STATUS_SUCCESS
                        ],['appid'=>$this->appid]);

                        // 发布
                        $app = app(Application::class);  //获取已绑定的第三方平台实例
                        $mapp = $app->miniProgram($applet['appid'], $applet['refresh_token']);
                        $mapp->rebind('cache', app('cache'));
                        $mapp->code->release();
                        break;
                    case 'weapp_audit_fail':
                         Applet::update([
                             'audit_time'=>$payload['FailTime'],
                             'audit_status'=>Applet::AUDIT_STATUS_FAIL
                         ],['appid'=>$this->appid]);
                        // Reason FailTime ScreenShot
                        break;
                    case 'weapp_audit_delay':
                        //Reason DelayTime
                        Applet::update([
                            'audit_time'=>$payload['DelayTime'],
                            'audit_status'=>Applet::AUDIT_STATUS_DELAY
                        ],['appid'=>$this->appid]);
                        break;
                }
                break;
            //接收普通消息: https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140453
            case 'text':
                Log::write('text message','debug');
                if ($res = Factory::make($payload)->reply()) {
                    Log::write($res,'debug');
                    return $res;
                }
                return "success";//"Content: " . $payload['Content'];
                break;
            case 'image':
               // return "MediaId: " . $payload['MediaId'] . "\nPicUrl: " . $payload['PicUrl'];
                break;
            case 'voice':
                //return "MediaId: " . $payload['MediaId'] . "\nFormat: " . $payload['Format'] . "\nRecognition: " . $payload['Recognition'];
                break;
            case 'video':
            case 'shortvideo':
                //return "MediaId: " . $payload['MediaId'] . "\nThumbMediaId: " . $payload['ThumbMediaId'];
                break;
            case 'location':
               //return "Location_X: " . $payload['Location_X'] . "\nLocation_Y: " . $payload['Location_Y'] . "\nScale: " . $payload['Scale'] . "\nLabel: " . $payload['Label'];
                break;
            case 'link':
                //return "Title: " . $payload['Title'] . "\nDescription: " . $payload['Description'] . "\nUrl: " . $payload['Url'];
                break;
            default:
                //return $payload['MsgType'];
                break;
        }*/
    }
}

