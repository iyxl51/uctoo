<?php

namespace catchAdmin\miniapp\controller;

use app\BaseController;
use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\miniapp\model\WechatCloud as wechatCloudModel;
use catchAdmin\wechatopen\model\Applet;
use catchAdmin\wechatopen\model\AdminApplet;
use catcher\CatchUpload;
use EasyWeChat\OpenPlatform\Application;
use think\exception\ValidateException;
use think\facade\Log;
use think\helper\Str;
use uctoo\ThinkEasyWeChat\MiniProgram\CloudFile\Client;
use uctoo\ThinkEasyWeChat\MiniProgram\CloudFile\ServiceProvider as CloudFileServiceProvider;
use uctoo\ThinkEasyWeChat\OpenPlatform\Cloud\CloudStaticStore\ServiceProvider;
use uctoo\validate\File;

class WechatStaticStore extends CatchController
{
    protected $wechatCloudModel;

    public function __construct(WechatCloudModel $wechatCloudModel)
    {
        $this->wechatCloudModel = $wechatCloudModel;
    }

    /**
     * 引入微信控制器的traits
     */
    use \uctoo\library\traits\Wechatopen;
    
    /**
     * 列表
     * @time 2021年03月23日 19:48
     * @param Request $request 
     */
    public function index(Request $request) : \think\Response
    {
        return CatchResponse::paginate($this->wechatCloudModel->getList());
    }
    
    /**
     * 保存信息
     * @time 2021年03月23日 19:48
     * @param Request $request 
     */
    public function save(Request $request) : \think\Response
    {
        return CatchResponse::success($this->wechatCloudModel->storeBy($request->post()));
    }
    
    /**
     * 读取
     * @time 2021年03月23日 19:48
     * @param $id 
     */
    public function read($id) : \think\Response
    {
        return CatchResponse::success($this->wechatCloudModel->findBy($id));
    }
    
    /**
     * 更新
     * @time 2021年03月23日 19:48
     * @param Request $request 
     * @param $id
     */
    public function update(Request $request, $id) : \think\Response
    {
        return CatchResponse::success($this->wechatCloudModel->updateBy($id, $request->post()));
    }
    
    /**
     * 删除
     * @time 2021年03月23日 19:48
     * @param $id
     */
    public function delete($id) : \think\Response
    {
        return CatchResponse::success($this->wechatCloudModel->deleteBy($id));
    }

    /**
     * 开通静态网站
     * @param Request $request
     * @param Application $app
     * @param string $env
     */
    public function createstaticstore(Request $request,Application $app) : \think\Response
    {       //查询管理员当前选中的applet
        $env = $request->post('env');
        Log::write($env,'debug');
            $adminApplet = AdminApplet::where('creator_id','=',$request->user()->id)->find();
            //查询applet
            $applet = Applet::where('appid','=',$adminApplet['appid'])->find();

            if($applet){
                $cloud_env = $app->register(new ServiceProvider)->cloud_env;
                $res = $cloud_env->createstaticstore($env);
                Log::write($res,'debug');

                return CatchResponse::success($res,'开通静态网站成功');
            }else{
                return CatchResponse::fail('请管理员先选中要操作的小程序');
            }
    }

    /**
     * 查看静态网站状态
     * @param Request $request
     * @param Application $app
     */
    public function describestaticstore(Request $request,Application $app) : \think\Response
    {       //查询管理员当前选中的applet
        $env = $request->post('env');
        Log::write($request->user()->id,'debug');
        $adminApplet = AdminApplet::where('creator_id','=',$request->user()->id)->find();
        //查询applet
        $applet = Applet::where('appid','=',$adminApplet['appid'])->find();
        Log::write($applet,'debug');
        if($applet){
            $cloud_env = $app->register(new ServiceProvider)->cloud_env;

            $res = $cloud_env->describestaticstore($env);

                Log::write($res,'debug');
                return CatchResponse::success($res,'查看静态网站状态成功');
        }else{
            return CatchResponse::fail('请管理员先选中要操作的小程序');
        }
    }

    /**
     * 获取静态网站文件列表
     * @param Request $request
     * @param Application $app
     */
    public function staticfilelist(Request $request,Application $app) : \think\Response
    {       //查询管理员当前选中的applet
        $env = $request->post('env');
        Log::write($env,'debug');
        $adminApplet = AdminApplet::where('creator_id','=',$request->user()->id)->find();
        //查询applet
        $applet = Applet::where('appid','=',$adminApplet['appid'])->find();

        if($applet){
            $cloud_env = $app->register(new ServiceProvider)->cloud_env;
            $res = $cloud_env->staticfilelist($env);
            Log::write($res,'debug');
            return CatchResponse::success($res,'获取静态网站文件列表成功');

        }else{
            return CatchResponse::fail('请管理员先选中要操作的小程序');
        }
    }

    /**
     * 上传静态网站文件
     * @param Request $request
     * @param Application $app
     */
    public function staticuploadfile(Request $request, CatchUpload $upload ,Application $app) : \think\Response
    {
        //查询管理员当前选中的applet
        $env = "weiyohotest-0gfa1ckv386bfde1";//$env = $request->post('env');
        //$filename = $request->post('filename');
        $file = $request->file();
        $images = $file['image'];
        $upfile = $upload->setDriver(CatchUpload::LOCAL)->checkImages($file)->multiUpload($file['image']);
        $filename = $file['image']->getOriginalName();
        if (empty($images)) {
            return CatchResponse::fail('缺少参数');
        }
        try{
            $cloud_env = $app->register(new ServiceProvider)->cloud_env;
            $path = $upfile[0];
            $res = $cloud_env->uploadFile($env,$filename,$path);
        }catch (ValidateException $e){
            return CatchResponse::fail('请管理员先选中要操作的小程序');
        }catch (\Exception $e){
            return CatchResponse::fail('请管理员先选中要操作的小程序');
        }
        return CatchResponse::success('','上传静态网站文件成功');
    }
}