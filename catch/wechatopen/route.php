<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2021 http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: JaguarJack [ njphper@gmail.com ]
// +----------------------------------------------------------------------

// wechatopen路由// wechatopen路由;
$router->group(function () use ($router){
    $router->resource('wechatopen', '\catchAdmin\wechatopen\controller\Wechatopen');
	// applet路由
	$router->resource('applet', '\catchAdmin\wechatopen\controller\Applet');
    // 切换应用
    $router->get('applet/setapplet/<id>', '\catchAdmin\wechatopen\controller\Applet@setApplet');
    // 获取当前选中应用
    $router->get('admin/applet/<creator_id>', '\catchAdmin\wechatopen\controller\AdminApplet@adminApplet');
	// adminApplet路由
	$router->resource('admin/applet', '\catchAdmin\wechatopen\controller\AdminApplet');
    // 扫码授权回跳页
    $router->get('wechatauth', '\catchAdmin\wechatopen\controller\Wechatauth');
	// wechatopenUsers路由
	$router->resource('wechatopenUsers', '\catchAdmin\wechatopen\controller\WechatopenUsers');
    //微信扫码后获取后台帐号
    $router->get('wechatlogin/wechatoauth', '\catchAdmin\wechatopen\controller\WechatLogin@wechatoauth');
    //微信扫码后登录后台帐号
    $router->post('wechatlogin/wechatlogin', '\catchAdmin\wechatopen\controller\WechatLogin@wechatlogin');
    //微信扫码后注册后台帐号
    $router->post('wechatlogin/wechatregist', '\catchAdmin\wechatopen\controller\WechatLogin@wechatregist');
});