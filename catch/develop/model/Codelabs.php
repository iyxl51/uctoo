<?php

namespace catchAdmin\develop\model;

use catcher\base\CatchModel as Model;
use catchAdmin\permissions\model\DataRangScopeTrait;
/**
 *
 * @property int $id
 * @property string $command
 * @property string $arguments
 * @property string $data_structure
 * @property string $template_code
 * @property string $config_data
 * @property string $algorithm
 * @property string $result
 * @property string $input
 * @property string $output
 * @property string $remark
 * @property string $template_file
 * @property string $data_source
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 * @property int $creator_id
 */
class Codelabs extends Model
{
    use DataRangScopeTrait;
    
    public $field = [
        //
        'id',
        // 命令名称
        'command',
        // 命令参数
        'arguments',
        // 数据结构
        'data_structure',
        // 模板代码
        'template_code',
        // 用户配置
        'config_data',
        // 算法
        'algorithm',
        // 结果
        'result',
        // 输入配置
        'input',
        // 输出配置
        'output',
        // 备注
        'remark',
        // 模板文件
        'template_file',
        // 数据源
        'data_source',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除字段
        'deleted_at',
        // 创建人ID
        'creator_id',
    ];
    
    public $name = 'codelabs';

    /**
     * 查询列表
     *
     * @time 2020年04月28日
     * @return mixed
     */
    public function getList()
    {
        // 不分页
        if (property_exists($this, 'paginate') && $this->paginate === false) {
            return $this->dataRange()->catchSearch()
                ->field('*')
                ->catchOrder()
                ->creator()
                ->select();
        }

        // 分页列表
        return $this->dataRange()->catchSearch()
            ->field('*')
            ->catchOrder()
            ->creator()
            ->paginate();
    }
}