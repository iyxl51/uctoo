<?php

namespace catchAdmin\wechatopen\controller;

use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\wechatopen\model\WechatopenUsers as WechatopenUsersModel;

class WechatopenUsers extends CatchController
{
    protected $WechatopenUsersModel;
    
    public function __construct(WechatopenUsersModel $WechatopenUsersModel)
    {
        $this->WechatopenUsersModel = $WechatopenUsersModel;
    }
    
    /**
     * 列表
     * @time 2021年05月31日 18:52
     * @param Request $request 
     */
    public function index(Request $request) : \think\Response
    {
        return CatchResponse::paginate($this->WechatopenUsersModel->getList());
    }
    
    /**
     * 保存信息
     * @time 2021年05月31日 18:52
     * @param Request $request 
     */
    public function save(Request $request) : \think\Response
    {
        return CatchResponse::success($this->WechatopenUsersModel->storeBy($request->post()));
    }
    
    /**
     * 读取
     * @time 2021年05月31日 18:52
     * @param $id 
     */
    public function read($id) : \think\Response
    {
        return CatchResponse::success($this->WechatopenUsersModel->findBy($id));
    }
    
    /**
     * 更新
     * @time 2021年05月31日 18:52
     * @param Request $request 
     * @param $id
     */
    public function update(Request $request, $id) : \think\Response
    {
        return CatchResponse::success($this->WechatopenUsersModel->updateBy($id, $request->post()));
    }
    
    /**
     * 删除
     * @time 2021年05月31日 18:52
     * @param $id
     */
    public function delete($id) : \think\Response
    {
        return CatchResponse::success($this->WechatopenUsersModel->deleteBy($id));
    }
}