<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Patrick <contact@uctoo.com>
// +----------------------------------------------------------------------
namespace uctoo\util\exception;

use think\Exception;


class ShopException extends Exception
{
    public function __construct($message = "", $code = 0, $data = [])
    {
        parent::__construct($message, $code);
        $this->setData('SHOP',$data);
    }

    public function __toString()
    {
        return __CLASS__.":[{$this->code}]:{$this->message}";
    }
}