<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Patrick <contact@uctoo.com>
// +----------------------------------------------------------------------

namespace uctoo\util\EventHandler\openPlatform;

use catchAdmin\wechatopen\model\Applet;
use think\facade\Cache;
use uctoo\ThinkEasyWeChat\Facade;
use EasyWeChat\Kernel\Contracts\EventHandlerInterface;
use EasyWeChat\OpenPlatform\Application;

class AuthorizedEventHandler implements EventHandlerInterface
{
    /**
     * @var Application
     */
    protected $app;
    protected $admin_id;
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    //已经在授权回跳地址处理过了，这里消息通知就不处理了，而且这里没法获取到授权者帐号ID
    public function handle($payload = null)
    {
        /*
        $auth = $this->app->handleAuthorize($payload['AuthorizationCode']);
        $app_info = $this->app->getAuthorizer($payload['AuthorizerAppid']);
        $data = $app_info;
        $appletModel = new Applet();
        $appletModel->startTrans();

        $applet = $appletModel->where(['appid'=>$payload['AuthorizerAppid']])->find();
        $data['authorization_info']['access_token_overtime']=time()+$auth['authorization_info']['expires_in']-1500;
        //获取公众号信息

        //授权成功，保存帐号信息
        if($applet){   //已授权过，更新授权信息

            $db_data['admin_id'] = '';
            $db_data['name'] = $app_info['authorizer_info']['nick_name'];
            $db_data['wechatapplet_id'] = md5($payload['AuthorizerAppid']);
            $db_data['principal'] = $app_info['authorizer_info']['principal_name'];
            $db_data['original'] = $app_info['authorizer_info']['user_name'];
            if (array_key_exists('head_img', $app_info['authorizer_info'])) {
                $db_data['headface_image'] = $app_info['authorizer_info']['head_img'];
            }else{
                $db_data['headface_image'] = '';
            }
            $db_data['qrcode_image'] = $app_info['authorizer_info']['qrcode_url'];
            $db_data['status'] = Applet::STATUS_AUTHORIZED;
            $db_data['service_type_info'] = json_encode($app_info['authorizer_info']['service_type_info']);
            $db_data['verify_type_info'] = json_encode($app_info['authorizer_info']['verify_type_info']);
            $db_data['business_info'] = json_encode($app_info['authorizer_info']['business_info']);
            $db_data['func_info'] = json_encode($app_info['authorization_info']['func_info']);
            $db_data['authorizer_access_token'] = $auth['authorization_info']['authorizer_access_token'];
            $db_data['access_token_overtime'] = $data['authorization_info']['access_token_overtime'];
            $db_data['authorizer_refresh_token'] = $auth['authorization_info']['authorizer_refresh_token'];
            if (array_key_exists('MiniProgramInfo', $app_info['authorizer_info'])) {   //是否是小程序授权
                $db_data['miniprograminfo'] = json_encode($app_info['authorizer_info']['MiniProgramInfo']);
                $db_data['signature'] = $app_info['authorizer_info']['signature'];
                $db_data['typedata'] = 'miniapp';
            }else{
                $db_data['wechat'] = $app_info['authorizer_info']['alias'];
                if($db_data['service_type_info'] == 0 || $db_data['service_type_info'] == 1){
                    $db_data['typedata'] = 'serv_account';
                }else{
                    $db_data['typedata'] = 'sub_account';
                }
            }
            $result = $applet->save($db_data);
        }else{       //未授权过，新增授权帐号
            $db_data['appid'] = $app_info['authorization_info']['authorizer_appid'];
            $db_data['wechatapplet_id'] = md5($payload['AuthorizerAppid']);
            $db_data['admin_id'] = '';
            $db_data['name'] = $app_info['authorizer_info']['nick_name'];
            $db_data['principal'] = $app_info['authorizer_info']['principal_name'];
            $db_data['original'] = $app_info['authorizer_info']['user_name'];
            if (array_key_exists('head_img', $app_info['authorizer_info'])) {
                $db_data['headface_image'] = $app_info['authorizer_info']['head_img'];
            }else{
                $db_data['headface_image'] = '';
            }
            $db_data['qrcode_image'] = $app_info['authorizer_info']['qrcode_url'];
            $db_data['status'] = Applet::STATUS_AUTHORIZED;
            $db_data['service_type_info'] = json_encode($app_info['authorizer_info']['service_type_info']);
            $db_data['verify_type_info'] = json_encode($app_info['authorizer_info']['verify_type_info']);
            $db_data['business_info'] = json_encode($app_info['authorizer_info']['business_info']);
            $db_data['func_info'] = json_encode($app_info['authorization_info']['func_info']);
            $db_data['authorizer_access_token'] = $auth['authorization_info']['authorizer_access_token'];
            $db_data['access_token_overtime'] = $data['authorization_info']['access_token_overtime'];
            $db_data['authorizer_refresh_token'] = $auth['authorization_info']['authorizer_refresh_token'];
            if (array_key_exists('MiniProgramInfo', $app_info['authorizer_info'])) {   //是否是小程序授权
                $db_data['miniprograminfo'] = json_encode($app_info['authorizer_info']['MiniProgramInfo']);
                $db_data['signature'] = $app_info['authorizer_info']['signature'];
                $db_data['typedata'] = 'miniapp';
            }else{
                $db_data['wechat'] = $app_info['authorizer_info']['alias'];
                if($db_data['service_type_info'] == 0 || $db_data['service_type_info'] == 1){
                    $db_data['typedata'] = 'serv_account';
                }else{
                    $db_data['typedata'] = 'sub_account';
                }
            }
            $result = $appletModel->save($db_data);
        }

        if($result === false){
            $appletModel->rollBack();
        }
        // 提交事务
        $appletModel->commit();
        */
    }

}