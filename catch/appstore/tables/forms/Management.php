<?php
namespace catchAdmin\appstore\tables\forms;

use catcher\library\form\Form;

class Management extends Form
{
    public function fields(): array
    {
        // TODO: Implement fields() method.
        return [
            self::input('product_id', 'product_id'),
            self::input('out_product_id', 'out_product_id'),
            self::input('title', '标题')->required(),
            self::input('sub_title', '副标题'),
            self::input('head_img', '主图'),
            self::input('desc_info', '商品详情'),
            self::input('brand_id', '品牌'),
            self::input('min_price', 'SKU最小价格'),
            self::input('cats', '类目'),
            self::input('attrs', '属性'),
            self::input('model', '商品型号'),
            self::input('shopcat', '分类'),
            self::input('skus', 'skus'),
            self::input('sales_count', '销量'),
            self::input('template_id', 'template_id'),
            self::input('ext_json', 'ext_json'),
            self::input('memo', '备注'),
            self::input('ext_attrs', 'ext_attrs'),
            self::input('edit_status', '草稿箱状态'),

            self::radio('status', '状态')->value(0)->options(
                self::options()->add('初始值', 0)->add('上架', 5)->add('回收站', 6)->add('逻辑删除', 9)->add('自主下架', 11)
                    ->add('售磬下架', 12)->add('违规下架/风控系统下架', 13)->render()
            ),

            self::number('weigh', '排序')->value(1)->min(1)->max(10000),
        ];
    }
}