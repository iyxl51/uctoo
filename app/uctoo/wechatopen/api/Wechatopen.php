<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Patrick <contact@uctoo.com>
// +----------------------------------------------------------------------

namespace app\uctoo\wechatopen\api;

use app\BaseController;
use uctoo\middleware\AppletTest;
use uctoo\middleware\Auth;
use EasyWeChat\Factory;
use catchAdmin\wechatopen\model\Applet;
use uctoo\ThinkEasyWeChat\OpenPlatform\Ability\Client;
use EasyWeChat\OpenPlatform\Application;
use uctoo\ThinkEasyWeChat\MiniShop\Base\ServiceProvider;
use think\facade\Log;

class Wechatopen extends BaseController
{
  //  protected $middleware = [
  //      Auth::class,
  //      AppletTest::class
  //  ];

    /**
     * applet 实例
     * @var \catchAdmin\wechatopen\model\Applet
     */
    protected $applet;

    // 初始化
    protected function initialize()
    {
        $appid = input('appid');
        $this->applet = Applet::where('appid',$appid)->find();
    }

    /**
     * product category  获取类目详情
     *
     * @param int $f_cat_id
     * @return array|Collection|object|ResponseInterface|string
     * @throws InvalidConfigException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function productCategoryGet(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $f_cat_id = input('f_cat_id');
        if(!is_numeric($f_cat_id)){
            $f_cat_id = 0;
        }
        $res = $minishop_base->productCategoryGet($f_cat_id);
        return $this->wxResult($res, 'minishop_base');
    }

    public function productBrandGet(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $res = $minishop_base->productBrandGet();
        return $this->wxResult($res, 'minishop_base');
    }

    public function productDeliveryGet_freight_template(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $res = $minishop_base->getFreightTemplate();
        return $this->wxResult($res, 'minishop_base');
    }

    public function productStoreGet_shopcat(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $res = $minishop_base->getShopcat();
        return $this->wxResult($res, 'minishop_base');
    }

    public function productSpuAdd(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $spu_data = input('post.');//'';
        $res = $minishop_base->productSpuAdd($spu_data);
        return $this->wxResult($res, 'minishop_base');
    }

    public function productSpuGet(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $body = input('post.');
        $res = $minishop_base->productSpuGet($body);
        return $this->wxResult($res, 'minishop_base');
    }

    public function productSpuGet_list(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $body = input('post.');
        $res = $minishop_base->productSpuGetlist($body);
        return $this->wxResult($res, 'minishop_base');
    }

    public function productSpuSearch(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $body = input('post.');
        $res = $minishop_base->productSpuSearch($body);
        Log::write($res,'debug');
        return $this->success('ok',$res);
       // $this->wxResult($res, 'minishop_base');  //官方文档与实际返回数据结构不一致
    }

    public function productSpuUpdate(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $body = input('post.');
        $res = $minishop_base->productSpuUpdate($body);
        return $this->wxResult($res, 'minishop_base');
    }

    public function productSpuListing(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $body = input('post.');
        $res = $minishop_base->productSpuListing($body);
        return $this->wxResult($res, 'minishop_base');
    }

    public function productSpuDelisting(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base;
        $body = input('post.');
        $res = $minishop_base->productSpuDelisting($body);
        return $this->wxResult($res, 'minishop_base');
    }

    public function productStoreGet_info(Application $app){
        $miniapp = $app->miniProgram($this->applet['appid'], $this->applet['authorizer_refresh_token']);
        $minishop_base = $miniapp->register(new ServiceProvider)->minishop_base; //
        $spu_data_json = '{
    "out_product_id": "1234566",
    "title": "小霸王 Nintendo Switch 国行续航增强版 NS家用体感游戏机掌机 便携掌上游戏机 红蓝主机",
    "sub_title": "JD自营更放心】【国行Switch，更安心的保修服务，更快的国行服务器】一台主机三种模式，游戏掌机，随时随地，一起趣玩。",
    "head_img": 
    [
        "http://img10.360buyimg.com/n1/s450x450_jfs/t1/85865/39/13611/488083/5e590a40E4bdf69c0/55c9bf645ea2b727.jpg"
    ],
    "desc_info": 
    {
        "imgs": 
        [
            "http://img10.360buyimg.com/n1/s450x450_jfs/t1/85865/39/13611/488083/5e590a40E4bdf69c0/55c9bf645ea2b727.jpg"
        ]
    },
    "brand_id": 2100000000,
    "cats": 
    [
        {
            "cat_id": 6033,
            "level": 1
        },
        {
            "cat_id": 6057,
            "level": 2
        },
        {
            "cat_id": 6091,
            "level": 3
        }
    ],
    "attrs": 
    [
        {
            "attr_key": "商品毛重",
            "attr_value": "380g"
        },
        {
            "attr_key": "商品产地",
            "attr_value": "中国大陆"
        }
    ],
    "model": "国行续航增强版",
    "express_info": 
    {
        "template_id": 5189
    },
    "skus":
    [
        {
            "out_product_id": "1234566",
            "out_sku_id": "1024",
            "thumb_img": "http://img10.360buyimg.com/n1/s450x450_jfs/t1/100778/17/13648/424215/5e590a40E2d68e774/e171d222a0c9b763.jpg",
            "sale_price": 1300,
            "market_price": 1500,
            "stock_num": 100,
            "sku_code": "A24525252",
            "barcode": "13251454",
            "sku_attrs": 
            [
                {
                    "attr_key": "选择颜色",
                    "attr_value": "红蓝主机"
                },
                {
                    "attr_key": "选择套装",
                    "attr_value": "主机+保护套"
                }
            ]
        }
    ]
}';
        $sku_data_json = '{
       "out_product_id": "1234566",
       "out_sku_id": "1024",
       "thumb_img": "http://img10.360buyimg.com/n1/s450x450_jfs/t1/100778/17/13648/424215/5e590a40E2d68e774/e171d222a0c9b763.jpg",
       "sale_price": 1300,
       "market_price": 1500,
       "stock_num": 100,
       "sku_code": "A24525252",
       "barcode": "13251454",
       "sku_attrs": 
       [
           {
               "attr_key": "选择颜色",
               "attr_value": "蓝白主机"
           },
           {
               "attr_key": "选择套装",
               "attr_value": "主机+耳机"
           }
       ]
}';
        $sku_price_json = '{
    "start_pay_time": "2020-08-01 12:05:25",
    "end_pay_time": "2020-08-30 12:05:25",
    "status": 20,
    "page": 1,
    "page_size": 10
}';
        $spu_data = json_decode($spu_data_json,true);
        $sku_data = json_decode($sku_data_json,true);
        $sku_price = json_decode($sku_price_json,true);

        $res = $minishop_base->productStoreGetinfo();
        Log::write($res,'debug');
        //$this->request->mapp->registerProviders([\uctoo\ThinkEasyWeChat\OpenPlatform\Authorizer\MiniProgram\Basic\ServiceProvider::class]);
        /**
         * @var \app\util\src\OpenPlatform\Authorizer\MiniProgram\Basic\Client $basic
         */
        //$basic = $this->request->mapp->basic;
        //$res = $basic->checkVerifyNickname('我是谁');
        //$this->error('',$res);
        return $this->wxResult($res, 'minishop_base');
    }


}